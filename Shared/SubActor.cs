﻿using Akka.Actor;

namespace Shared
{
    public class SubActor : UntypedActor
    {
        private Guid ActorId { get; init; }

        public SubActor(string someArgs, long otherArgs)
        {
            ActorId = Guid.NewGuid();
            Console.WriteLine($"Constructing actor with {someArgs}, {otherArgs}");
        }

        protected override void PreStart()
        {
            Console.WriteLine($"Subactor {ActorId} has been started");
        }

        protected override void PostStop()
        {
            Console.WriteLine($"Subactor {ActorId} has been stopped");
        }

        protected override void OnReceive(object message)
        {
            if (message is long)
            {
                Console.Write(".");
            }
            else
            {
                Console.WriteLine($"{Self.Path.ToStringWithAddress()} got {message}");
                Sender.Tell("Hello");
            }
        }
    }
}