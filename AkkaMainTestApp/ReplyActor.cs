﻿using Akka.Actor;

namespace AkkaMainTestApp
{
    public class ReplyActor : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            Console.WriteLine($"Message from {Sender.Path} - {message}");
        }
    }
}
