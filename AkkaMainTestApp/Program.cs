﻿using Akka.Actor;
using Akka.Configuration;
using Akka.Routing;
using AkkaMainTestApp;
using Petabridge.Cmd.Host;
using Petabridge.Cmd.Remote;
using Shared;

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Starting first system");

var config = ConfigurationFactory.ParseString(File.ReadAllText("app.hocon"));

using (var system = ActorSystem.Create("main", config))
{
    var reply = system.ActorOf<ReplyActor>("reply");

    var local = system.ActorOf(Props.Create(() => new SubActor("Hello local actor", 123)).WithRouter(FromConfig.Instance), "localactor");

    var remote = system.ActorOf(Props.Create(() => new SubActor("Hello remote actor", 123)).WithRouter(FromConfig.Instance), "remoteactor");

    var pbm = PetabridgeCmd.Get(system);
    pbm.RegisterCommandPalette(new RemoteCommands());
    pbm.Start();

    Console.ReadLine();

    Parallel.For(1, 10000, (i, state) =>
    {
        local.Tell($"Local message {i}", reply);
        remote.Tell($"Remote message {i}", reply);
    });

    Console.ReadLine();
}
