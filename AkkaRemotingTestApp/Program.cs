﻿using Akka.Actor;
using Akka.Configuration;

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Starting second system");

var config = ConfigurationFactory.ParseString(File.ReadAllText("app.hocon"));

using (ActorSystem.Create("remotesystem", config))
{
    Console.ReadLine();
}
